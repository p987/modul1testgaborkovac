DROP SCHEMA IF EXISTS biblioteka;
CREATE SCHEMA biblioteka DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE biblioteka;

CREATE TABLE clan (
	id BIGINT AUTO_INCREMENT,
	korisničkoIme VARCHAR(75) NOT NULL,
	maxKnjiga INT NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE knjige (
	id BIGINT AUTO_INCREMENT,
	ISBN VARCHAR(75) NOT NULL UNIQUE,
    autor VARCHAR(100) NOT NULL,
    naslov VARCHAR(100) NOT NULL,
	datum DATE,
    clanId BIGINT,
	PRIMARY KEY(id), 
    FOREIGN KEY(clanId) REFERENCES clan(id)
);

# članovi: korisničko ime, maksimalni broj zaduženih knjiga
INSERT INTO clan (id, korisničkoIme, maxKnjiga) VALUES (1, 'pera', 3);
INSERT INTO clan (id, korisničkoIme, maxKnjiga) VALUES (2, 'mika', 4);
INSERT INTO clan (id, korisničkoIme, maxKnjiga) VALUES (3, 'zika', 4);

#knjige: ISBN, autor, naslov, datum zaduženja, član koji je zadužio knjigu
INSERT INTO knjige (ISBN, autor, naslov, datum, clanId) VALUES ('0001', 'Lav Tolstoj', 'Rat i mir', '2023-09-22', 2);
INSERT INTO knjige (ISBN, autor, naslov, datum, clanId) VALUES ('0002', 'Lav Tolstoj', 'Rat i mir', '2023-10-22', 1);
INSERT INTO knjige (ISBN, autor, naslov, datum, clanId) VALUES ('0003', 'Lav Tolstoj', 'Ana Karenjina', '2023-09-09', 1);
INSERT INTO knjige (ISBN, autor, naslov, datum, clanId) VALUES ('0004', 'Lav Tolstoj', 'Ana Karenjina', '2023-09-15', 2);
INSERT INTO knjige (ISBN, autor, naslov, datum, clanId) VALUES ('0005', 'Lav Tolstoj', 'Ana Karenjina', '2023-09-25', 3);
INSERT INTO knjige (ISBN, autor, naslov, datum, clanId) VALUES ('0006', 'Džejn Ostin', 'Gordost i predrasuda', '2023-09-09', 3);
INSERT INTO knjige (ISBN, autor, naslov, datum, clanId) VALUES ('0007', 'Džejn Ostin', 'Lejdi Suzan', null, null);
INSERT INTO knjige (ISBN, autor, naslov, datum, clanId) VALUES ('0008', 'Džejn Ostin', 'Razum i osećajnost', '2023-09-09', 3);
INSERT INTO knjige (ISBN, autor, naslov, datum, clanId) VALUES ('0009', 'Džejn Ostin', 'Razum i osećajnost', '2023-09-19', 2);
INSERT INTO knjige (ISBN, autor, naslov, datum, clanId) VALUES ('0010', 'Džejn Ostin', 'Razum i osećajnost', '2023-09-21', 1);
INSERT INTO knjige (ISBN, autor, naslov, datum, clanId) VALUES ('0011', 'Henri Džejms', 'Portret jedne dame', null, null);