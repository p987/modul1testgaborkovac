package DAO;

import java.util.Collection;

import model.Knjige;

public interface KnjigeDAO {
	
	public void izmena(Knjige karta) throws Exception;
	public Collection<Knjige> getAll() throws Exception;
	
}
