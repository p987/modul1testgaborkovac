package DAO;

import java.util.Collection;

import model.Clan;

public interface ClanDAO {

	public Clan get(String brojVoza) throws Exception;
	public Collection<Clan> getAll() throws Exception;

}
