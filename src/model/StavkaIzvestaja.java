package model;

import util.Konzola;

public class StavkaIzvestaja {
	private String autor;
	private int brojZaduzenihKnjiga;
	private Clan clan;
	
	
	public static int compareBrojPoziva(StavkaIzvestaja stavka1, StavkaIzvestaja stavka2) {
		return -Double.compare(stavka1.brojZaduzenihKnjiga, stavka2.brojZaduzenihKnjiga);
	}


	public StavkaIzvestaja(String autor, int brojZaduzenihKnjiga, Clan clan) {
		this.autor = autor;
		this.brojZaduzenihKnjiga = brojZaduzenihKnjiga;
		this.clan = clan;
	}
	
	
	private int komada() {
		int brojac = 0;
		for (Knjige itKnjige : clan.getKnjigeSet()) {
			if (itKnjige.getAutor().equals(autor)) {
				++brojac;
			}
		}
		return brojac;
	}
	

	@Override
	public String toString() {
		return "StavkaIzvestaja [autor=" + autor + ", brojZaduzenihKnjigaOdAutora=" + brojZaduzenihKnjiga + ", clan preuzeo najviše knjiga od autora=" + clan.getKorisnickoIme()
		+ ", clan ima kod sebe knjigu od autora =" + komada() + " komada ]";
	}


	public String getAutor() {
		return autor;
	}


	public void setAutor(String autor) {
		this.autor = autor;
	}


	public int getBrojZaduzenihKnjiga() {
		return brojZaduzenihKnjiga;
	}


	public void setBrojZaduzenihKnjiga(int brojZaduzenihKnjiga) {
		this.brojZaduzenihKnjiga = brojZaduzenihKnjiga;
	}


	public Clan getClan() {
		return clan;
	}


	public void setClan(Clan clan) {
		this.clan = clan;
	}
	
}
