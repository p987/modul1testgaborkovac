package model;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;


public class Clan {

	private final long  id;
	private String korisnickoIme;
	private int maxKnjiga;
	private Set<Knjige> knjigeSet = new LinkedHashSet<>();
	
	
	public Clan(long id, String korisnickoIme, int maxKnjiga) {
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.maxKnjiga = maxKnjiga;
	}


	@Override
	public String toString() {
		String str = "Clan [id=" + id + ", korisnickoIme=" + korisnickoIme + ", maxKnjiga=" + maxKnjiga	+ "]";
		return str;
	}
	
	
	public Set<Knjige> getKnjigeSet() {
		return Collections.unmodifiableSet(knjigeSet);
	}


	public void addAllKnjigeSet(Set<Knjige> knjigeSet) {
		this.knjigeSet.addAll(knjigeSet);
	}

	
	public void addKnjigeSet(Knjige knjige) {
		this.knjigeSet.add(knjige);
	}
	

	public String getKorisnickoIme() {
		return korisnickoIme;
	}


	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}


	public int getMaxKnjiga() {
		return maxKnjiga;
	}


	public void setMaxKnjiga(int maxKnjiga) {
		this.maxKnjiga = maxKnjiga;
	}


	public long getId() {
		return id;
	}


	@Override
	public int hashCode() {
		return Objects.hash(id);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Clan other = (Clan) obj;
		return id == other.id;
	}	
}
