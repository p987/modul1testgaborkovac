package model;

import java.time.LocalDate;
import java.util.Objects;


public class Knjige {

	private final long id;
	private String ISBN;
	private String autor;
	private String naslov;
	private LocalDate datumZaduzenja;
	private Clan clan;
	
	
	public Knjige(long id, String iSBN, String autor, String naslov, LocalDate datumZaduzenja, Clan clan) {
		this.id = id;
		ISBN = iSBN;
		this.autor = autor;
		this.naslov = naslov;
		this.datumZaduzenja = datumZaduzenja;
		this.clan = clan;
	}
	
	
	public boolean isDatumUOpsegu(LocalDate pocetak, LocalDate kraj) {
	return datumZaduzenja.compareTo(pocetak) >= 0 && datumZaduzenja.compareTo(kraj) <= 0;
}
	

	@Override
	public String toString() {
		return "Knjige [id=" + id + ", ISBN=" + ISBN + ", autor=" + autor + ", naslov=" + naslov + ", datumZaduzenja="
				+ (datumZaduzenja != null ? datumZaduzenja : "knjiga nije zaduena") + ", clan=" + (clan != null ? clan : "") + "]";
	}


	public String getISBN() {
		return ISBN;
	}


	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}


	public String getAutor() {
		return autor;
	}


	public void setAutor(String autor) {
		this.autor = autor;
	}


	public String getNaslov() {
		return naslov;
	}


	public void setNaslov(String naslov) {
		this.naslov = naslov;
	}


	public LocalDate getDatumZaduzenja() {
		return datumZaduzenja;
	}


	public void setDatumZaduzenja(LocalDate datumZaduzenja) {
		this.datumZaduzenja = datumZaduzenja;
	}


	public Clan getClan() {
		return clan;
	}


	public void setClan(Clan clan) {
		this.clan = clan;
	}


	public long getId() {
		return id;
	}


	@Override
	public int hashCode() {
		return Objects.hash(ISBN, id);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Knjige other = (Knjige) obj;
		return Objects.equals(ISBN, other.ISBN) && id == other.id;
	}
	
	
	
}
