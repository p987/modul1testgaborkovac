package UI;

import java.util.Collection;

import DAO.ClanDAO;
import model.Knjige;
import model.Clan;
import util.Konzola;



public class ClanUI {

	private static ClanDAO clanDAO;
	
	public static void setVozDAO(ClanDAO vozDAO) {
		ClanUI.clanDAO = vozDAO;
	}
	
	
	public static Clan pronalazenje() throws Exception {
		prikazSvih();

		String korisnickoIme = Konzola.ocitajString("Unesite korisničko ime: ");
		System.out.println();

		Clan voz = clanDAO.get(korisnickoIme);
		if (voz == null)
			Konzola.prikazi("Korisničko ime nije pronađeno!");

		return voz;
	}
	
	
	public static void prikaz() {
		try {
			Clan clan = pronalazenje();
			if (clan == null)
				return;
			System.out.println(clan);
			System.out.println("Zadužene knjige: ");
			for (Knjige itKarta : clan.getKnjigeSet()) {
				System.out.println(itKarta.getNaslov());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Došlo je do greške!");
		}
	}

	public static void prikazSvih() {
		try {
			Collection<Clan> vozKolekcija = clanDAO.getAll();

			System.out.println();
			for (Clan itVoz: vozKolekcija) {
				System.out.println(itVoz);
//				for (Knjige itPorudzbina: itVoz.getKnjigeSet()) {
//					System.out.println(itPorudzbina.getNaslov());
//				}
//				System.out.println("\n\n");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Došlo je do greške!");
		}
	}

	

	

}
