package UI;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import DAO.KnjigeDAO;
import model.Clan;
import model.Knjige;
import util.Konzola;

public class KnjigeUI {

	private static KnjigeDAO knjigeDAO;
	
	public static void setKnjigeDAO(KnjigeDAO kartaDAO) {
		KnjigeUI.knjigeDAO = kartaDAO;
	}

	public static void prikazSvih() {
		try {
			Collection<Knjige> knjige = knjigeDAO.getAll();
			for (Knjige knjiga : knjige) {
				System.out.println(knjiga);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Došlo je do greške!");
		}
	}
	
	
	public static void dodavanje() {
		Set<Knjige> knjigeSet = new HashSet<>();
		Knjige knjiga = null;
		
		try {
			Clan clan = ClanUI.pronalazenje();
			
			if (clan == null) {
				return;
			}
			
			if (clan.getKnjigeSet().size() > clan.getMaxKnjiga()) { // pr. mesta(5) - size(5) <= 0
				System.out.println("Nemožete uzeti više knjiga!");
				System.out.println(clan);
				return;
			}
			
			prikazSvih();
			String ISBN = Konzola.ocitajString("Unesite ISBN knjige:");
			knjigeSet.addAll(knjigeDAO.getAll()) ;
			boolean proveraISBN = false;
			
			for (Knjige itKnjige : knjigeSet) {
				if (itKnjige.getISBN().equals(ISBN) && itKnjige.getDatumZaduzenja() != null) {
					System.out.println("Knjiga je već zadužena!");
					return;
				} else if (itKnjige.getISBN().equals(ISBN)) {
					knjiga = itKnjige;
					proveraISBN = true;
				}	
			}
			if (!proveraISBN) {
				System.out.println("Knjiga ne postoji sa ISBN: " + ISBN + " !");
				return;
			}

			LocalDate datum = LocalDate.now();
			knjiga.setClan(clan);
			knjiga.setDatumZaduzenja(datum);
			knjigeDAO.izmena(knjiga);
			System.out.println("Knjiga je uspešno zadužena!");
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Došlo je do greške!");
		}
	}


	

}
