package UI;

import java.sql.Connection;
import java.sql.DriverManager;

import DAO.KnjigeDAO;
import DAO.ClanDAO;
import DAOImpl.KnjigeDAOImpl;
import DAOImpl.ClanDAOImpl;
import util.Meni;
import util.Meni.FunkcionalnaStavkaMenija;
import util.Meni.IzlaznaStavkaMenija;
import util.Meni.StavkaMenija;




public class Application {
	private static void initDatabase() throws Exception {
		Connection conn = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/biblioteka?allowPublicKeyRetrieval=true&useSSL=false&serverTimezone=Europe/Belgrade", 
				"root", 
				"root");
		
		ClanDAO clanDAO = new ClanDAOImpl(conn);
		KnjigeDAO knjigaDAO = new KnjigeDAOImpl(conn);
		
		ClanUI.setVozDAO(clanDAO);
		KnjigeUI.setKnjigeDAO(knjigaDAO);
		IzvestajUI.setIzvestaj(knjigaDAO, clanDAO);
	}

	static {
		try {
			initDatabase();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Greï¿½ka pri povezivanju sa izvorom podataka!");
			
			System.exit(1);
		}
	}

	public static void main(String[] args) throws Exception {
		Meni.pokreni("Biblioteka", new StavkaMenija[] {
			new IzlaznaStavkaMenija("Izlaz"),
			new FunkcionalnaStavkaMenija("Prikaz svih knjiga") {

				@Override
				public void izvrsi() { KnjigeUI.prikazSvih(); }
				
			}, 
			new FunkcionalnaStavkaMenija("Prikaz jednog člana sa zaduženim knjigama") {

				@Override
				public void izvrsi() { ClanUI.prikaz(); }
				
			}, 
			new FunkcionalnaStavkaMenija("Zaduživanje knjige") {

				@Override
				public void izvrsi() { KnjigeUI.dodavanje(); }
				
			}, // poceo raditi
			new FunkcionalnaStavkaMenija("Izveštaj zaduženih knjiga po autoru knjiga") {

				@Override
public void izvrsi() { IzvestajUI.izvestavanje(); } // nisam izmenio
				
			}
		});
	}
}
