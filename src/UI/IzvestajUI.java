package UI;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import DAO.ClanDAO;
import DAO.KnjigeDAO;
import model.Clan;
import model.Knjige;
import model.StavkaIzvestaja;
import util.Konzola;

public class IzvestajUI {
	private static KnjigeDAO knjigaDAO;
	private static ClanDAO clanDAO;

	public static void setIzvestaj(KnjigeDAO knjigaDAO, ClanDAO clanDAO) {
		IzvestajUI.knjigaDAO = knjigaDAO;
		IzvestajUI.clanDAO = clanDAO;
	}

	public static void izvestavanje() {
		LocalDate pocetak = Konzola.ocitajDate("Unesite pocetak perioda");
		LocalDate kraj = Konzola.ocitajDate("Unesite kraj perioda");
//		LocalDate pocetak = LocalDate.parse("19.09.2023.", Konzola.getDateFormatter());
//		LocalDate kraj = LocalDate.parse("30.09.2023.", Konzola.getDateFormatter());
		try {
			Collection<Knjige> knjigeKolekcija = knjigaDAO.getAll();
			List<StavkaIzvestaja> stavkaIzvestajaList = new ArrayList<>();
			Set<String> autorKnjigeSet = new HashSet<>();
			Map<Clan, Integer> brojacClana = new HashMap<>(); // Kljuc je korisnickoIme a vrednost je brojac

			for (Knjige knjige : knjigeKolekcija) {
				autorKnjigeSet.add(knjige.getAutor()); // vadim sve autore knjige
			}

			for (String itAutorKnjige : autorKnjigeSet) {
				int brojacZaduzeneKnjige = 0;

				for (Knjige itKnjige : knjigeKolekcija) {

					boolean provera = itKnjige.getAutor().equals(itAutorKnjige);
					boolean provera1 = false;
					if (itKnjige.getDatumZaduzenja() != null) {
						provera1 = itKnjige.isDatumUOpsegu(pocetak, kraj);
					}

					boolean provera3 = itKnjige.getDatumZaduzenja() != null;
					if (itKnjige.getDatumZaduzenja() != null) {
						if ((itKnjige.getAutor().equals(itAutorKnjige)) && (itKnjige.isDatumUOpsegu(pocetak, kraj))) {						
							++brojacZaduzeneKnjige;
							if (brojacClana.get(itKnjige.getClan()) != null) { // ako postoji taj clan u mapi(clan je
																				// sad kljuc!)
								int brojac = brojacClana.get(itKnjige.getClan()); // vadim vrednost iz mape(intBrojac)
								brojacClana.put(itKnjige.getClan(), ++brojac); // vracam ++intBrojac za isti kljuc
							} else {
								brojacClana.put(itKnjige.getClan(), 1); // ako nema tog clana u mapi, onda pravim novi
							}

						}
					}
				}
				int najveciBrojac = 0;
				Clan clan = null;

				for (Clan key : brojacClana.keySet()) {
					int brojac = brojacClana.get(key); // vadim values iz mape
					if (brojac > najveciBrojac) {
						najveciBrojac = brojac;
						clan = key;
					}
				}
				//ovde napunim clan sa set knjiga, koja mu pripadaju
				if (clan != null) {
					Collection<Clan> clanList = clanDAO.getAll();
					for (Clan itClan : clanList) {
						if (itClan.equals(clan)) {
							clan.addAllKnjigeSet(itClan.getKnjigeSet());
						}
					}
					StavkaIzvestaja sti = new StavkaIzvestaja(itAutorKnjige, najveciBrojac, clan);
					stavkaIzvestajaList.add(sti);
				}
			}
			// sortiranje izveštaja
			stavkaIzvestajaList.sort(StavkaIzvestaja::compareBrojPoziva);

			// prikaz izveštaja
			System.out.println();
			for (StavkaIzvestaja itStavka : stavkaIzvestajaList) {
				System.out.println(itStavka);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Došlo je do greške!");
		}
	}
}
