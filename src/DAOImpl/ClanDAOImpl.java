package DAOImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import DAO.ClanDAO;
import model.Knjige;
import model.Clan;



public class ClanDAOImpl implements ClanDAO {

	private Connection conn;
	
	public ClanDAOImpl(Connection conn) {
		this.conn = conn;
	}
	
	@Override
	public Clan get(String korisnickoIme) throws Exception{
		Clan voz = null;
		
		String sql = "SELECT c.id AS ClanId, c.korisničkoIme, c.maxKnjiga, k.id, k.ISBN, k.autor, k.naslov, k.datum \n"
				+ "FROM clan c  \n"
				+ "LEFT JOIN knjige k ON c.id = k.clanId WHERE c.korisničkoIme = ?;";
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			stmt.setString(1, korisnickoIme);
			try (ResultSet rset = stmt.executeQuery()) {
				while (rset.next()) {
					int kolona = 0;
					long cId = rset.getLong(++kolona);
					String cKorisnickoIme = rset.getString(++kolona);
					int cMaxKnjiga = rset.getInt(++kolona);
					
					if (voz == null) {
						voz = new Clan(cId, cKorisnickoIme, cMaxKnjiga);
					}
					
					long kId = rset.getLong(++kolona);
					if (kId != 0) {
						String kISBN = rset.getString(++kolona);
						String kAutor = rset.getString(++kolona);
						String kNaslov = rset.getString(++kolona);
						LocalDate kDatum = null;
						if (rset.getDate(++kolona) != null) {
							kDatum = rset.getDate(kolona).toLocalDate();
						}		

						Knjige karta = new Knjige(kId, kISBN, kAutor, kNaslov, kDatum, voz);
						voz.addKnjigeSet(karta);
					}
				}
			}
		}
		return voz;
	}

	@Override
	public Collection<Clan> getAll() throws Exception{
		Map<Long, Clan> vozMap = new LinkedHashMap<>();
		
		String sql = "SELECT c.id AS ClanId, c.korisničkoIme, c.maxKnjiga, k.id, k.ISBN, k.autor, k.naslov, k.datum \n"
				+ "FROM clan c  \n"
				+ "LEFT JOIN knjige k ON c.id = k.clanId;";
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			try (ResultSet rset = stmt.executeQuery()) {
				while (rset.next()) {
					int kolona = 0;
					long cId = rset.getLong(++kolona);
					String cKorisnickoIme = rset.getString(++kolona);
					int cMaxKnjiga = rset.getInt(++kolona);
					
					Clan voz = vozMap.get(cId);
					if (voz == null) {
						voz = new Clan(cId, cKorisnickoIme, cMaxKnjiga);
						vozMap.put(voz.getId(), voz);
					}
					
					long kId = rset.getLong(++kolona);
					if (kId != 0) {
						String kISBN = rset.getString(++kolona);
						String kAutor = rset.getString(++kolona);
						String kNaslov = rset.getString(++kolona);
						LocalDate kDatum = null;
						if (rset.getDate(++kolona) != null) {
							kDatum = rset.getDate(kolona).toLocalDate();
						}

						Knjige karta = new Knjige(kId, kISBN, kAutor, kNaslov, kDatum, voz);
						voz.addKnjigeSet(karta);
					}
				}
			}
		}
		return vozMap.values();
	}
}
