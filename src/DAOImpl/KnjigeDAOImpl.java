package DAOImpl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import DAO.KnjigeDAO;
import model.Clan;
import model.Knjige;

;

public class KnjigeDAOImpl implements KnjigeDAO {
	
	private Connection conn;

	public KnjigeDAOImpl(Connection conn) {
		this.conn = conn;
	}

	
	@Override
	public Collection<Knjige> getAll() throws Exception{
		Map<Long, Knjige> vozMap = new LinkedHashMap<>();
		
		String sql = "SELECT k.id, k.ISBN, k.autor, k.naslov, k.datum, c.id AS ClanId, c.korisničkoIme, c.maxKnjiga \n"
				+ "FROM knjige k \n"
				+ "LEFT JOIN clan c ON k.clanId = c.id;";
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			try (ResultSet rset = stmt.executeQuery()) {
				while (rset.next()) {
					int kolona = 0;
					long kId = rset.getLong(++kolona);
					String kISBN = rset.getString(++kolona);
					String kAutor = rset.getString(++kolona);
					String kNaslov = rset.getString(++kolona);
										
					LocalDate kDatum = null;
					if (rset.getDate(++kolona) != null) {
						kDatum = rset.getDate(kolona).toLocalDate();
					}		
					
					Knjige knjige = vozMap.get(kId);
					if (knjige == null) {
						knjige = new Knjige(kId, kISBN, kAutor, kNaslov, kDatum, null);
						vozMap.put(knjige.getId(), knjige);
					}
					
					long cId = rset.getLong(++kolona);
					if (cId != 0) {
						String cKorisnickoIme = rset.getString(++kolona);
						int cMaxKnjiga = rset.getInt(++kolona);

						Clan clan = new Clan(cId, cKorisnickoIme, cMaxKnjiga);
						knjige.setClan(clan);
					}
				}
			}
		}
		return vozMap.values();
	}
	
//	@Override
	public void izmena(Knjige karta) throws Exception {
		String sql = "UPDATE knjige SET autor = ?, naslov = ?, datum = ?, clanId = ? WHERE (id = ?);";
		try (PreparedStatement stmt = conn.prepareStatement(sql)) {
			int param = 0;
			stmt.setString(++param, karta.getAutor());
			stmt.setString(++param, karta.getNaslov());
			stmt.setDate(++param, Date.valueOf(karta.getDatumZaduzenja()));
			
			stmt.setLong(++param, karta.getClan().getId());
			stmt.setLong(++param, karta.getId());
			

			stmt.executeUpdate();
		}
	}

}
